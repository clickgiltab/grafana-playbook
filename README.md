ansible-playbook -i hosts influxdb.yml

ansible-playbook -i hosts telegraf.yml

ansible-playbook -i hosts grafana.yml --vault-password-file vault-password-file

Протестировал на Debian9. За рамками тестового задания можно сделать плейбуки мультиплатформенными. 
